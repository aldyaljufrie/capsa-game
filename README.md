# Assets used in this project

- [Free Playing Cards Pack](https://assetstore.unity.com/packages/3d/props/tools/free-playing-cards-pack-154780)
- [Card shirts Lite](https://assetstore.unity.com/packages/2d/gui/card-shirts-lite-165698)
- [Medieval Cartoon Warriors](https://assetstore.unity.com/packages/3d/characters/medieval-cartoon-warriors-90079)
- [Cartoon FX Remaster Free](https://assetstore.unity.com/packages/vfx/particles/cartoon-fx-remaster-free-109565)
- [Kenney UI Pack (RPG Expansion)](https://www.kenney.nl/assets/ui-pack-rpg-expansion)
