using UnityEngine;
using System;
using System.Linq;
using System.Collections.Generic;
using static BigTwoBattle.Constants;

namespace BigTwoBattle
{
    public class Player : Hero
    {
        public bool isAI = true;
        public bool isPassed = false;
        public Transform hand;
        public ParticleSystem attackParticle;
        public ParticleSystem hitParticle;

        private Dictionary<CombinationTypes, List<CardCombination>> combinations = new();
        private List<int> cardValues;
        private int qhIndex;
        private CombinationTypes qhType;

        [HideInInspector] public List<Card> selectedCards = new();
        public CardCombination selectedCombination;
        public List<Card> cards;
        public event Action<Dictionary<CombinationTypes, List<CardCombination>>> OnCombinationChanged;

        private void OnEnable()
        {
            CardManager.Instance.onReset += ResetHand;
            GameManager.Instance.OnNextTurn += OnNextTurn;
            OnCombinationChanged += OnAnalyzeComplete;
        }

        private void OnDisable()
        {
            CardManager.Instance.onReset -= ResetHand;
            GameManager.Instance.OnNextTurn -= OnNextTurn;
            OnCombinationChanged -= OnAnalyzeComplete;
        }

        private void OnNextTurn(int index, bool newRound)
        {
            if (newRound) isPassed = false;
            if (index == ID)
            {
                stateMachine.ChangeState(attackState);
                if (!isPassed)
                    FindCombinations();
                else
                    PassTurn();
            }
            else
            {
                if (!isPassed)
                    stateMachine.ChangeState(idleState);
            }

        }

        private void OnAnalyzeComplete(Dictionary<CombinationTypes, List<CardCombination>> newCombs)
        {
            if (GameManager.Instance.playerTurn != ID) return;

            if (isAI)
            {
                CardCombination res = FindMove(CardManager.Instance.currentPlay);
                if (res != null)
                {
                    selectedCards = res.cards.ToList();
                    selectedCombination = res;
                    Invoke("SubmitCard", 2);
                }
                else
                    Invoke("PassTurn", 0.5f);
            }
        }

        public void AddCombination(CardCombination c)
        {
            if (combinations.ContainsKey(c.type) && !combinations[c.type].Contains(c))
                combinations[c.type].Add(c);
        }

        private void RemoveCombination(CardCombination c)
        {
            if (combinations.ContainsKey(c.type) && combinations[c.type].Contains(c))
            {
                combinations[c.type].Remove(c);
                OnCombinationChanged?.Invoke(combinations);
            }
        }

        public void AddSelectedCard(Card card)
        {
            if (selectedCards.Count >= 5)
            {
                foreach (Card c in selectedCards)
                    c.ToggleCard(false);
                selectedCards.Clear();
            }

            selectedCards.Add(card);
            selectedCombination = CardCombination.MakeCombination(selectedCards.ToArray());
            card.ToggleCard(true);
        }

        public void RemoveSelectedCard(Card card)
        {
            selectedCards.Remove(card);
            selectedCombination = CardCombination.MakeCombination(selectedCards.ToArray());
            card.ToggleCard(false);
        }

        public void SetPlayerHand(List<int> values)
        {
            isPassed = false;
            selectedCombination = null;
            selectedCards = new();
            cards = new List<Card>();
            cardValues = values;

            stateMachine.ChangeState(idleState);
            for (int i = 0; i < cardValues.Count; i++)
            {
                Card card = Card.Create(values[i], this, hand);
                cards.Add(card);
            }

            Card TD = cards.Find(x => x.value == Values.Three && x.suit == Suits.Diamond);
            if (TD)
            {
                GameManager.Instance.SetFirstTurn(ID);
                selectedCards.Add(TD);
                selectedCombination = CardCombination.MakeCombination(selectedCards.ToArray());
                stateMachine.ChangeState(attackState);
                Invoke("SubmitCard", 0.5f);
            }
        }

        private void FindCombinations()
        {
            combinations.Clear();
            Dictionary<CombinationTypes, List<CardCombination>> newCombs = Enum.GetValues(typeof(CombinationTypes)).Cast<CombinationTypes>().ToDictionary(k => k, _ => new List<CardCombination>());

            foreach (Card card in cards)
                newCombs[CombinationTypes.Single].Add(CardCombination.MakeCombination(new Card[] { card }));

            newCombs[CombinationTypes.Pair].AddRange(CardValidator.FindPairs(cards));
            newCombs[CombinationTypes.Triples].AddRange(CardValidator.FindTriples(cards));
            newCombs[CombinationTypes.FourOfAKind].AddRange(CardValidator.FindFourKind(cards));
            newCombs[CombinationTypes.Flush].AddRange(CardValidator.FindFlush(cards));
            newCombs[CombinationTypes.Straight].AddRange(CardValidator.FindStraight(cards));
            newCombs[CombinationTypes.StraightFlush].AddRange(CardValidator.FindStraightFlush(cards));

            if (newCombs[CombinationTypes.Triples].Count > 0 && newCombs[CombinationTypes.Pair].Count > 0)
                newCombs[CombinationTypes.FullHouse].AddRange(
                    CardValidator.FindFullHouse(
                        newCombs[CombinationTypes.Triples].ToArray(),
                        newCombs[CombinationTypes.Pair].ToArray())
                );

            combinations = newCombs;
            OnCombinationChanged?.Invoke(newCombs);
        }

        private CardCombination FindMove(CardCombination currentPlay)
        {
            if (GameManager.Instance.isFirstMove)
            {
                CombinationTypes[] types = combinations.Where(x => x.Value.Count > 0).ToDictionary(x => x.Key, x => x.Value).Keys.ToArray();
                CardCombination newComb = combinations[types.Max()][0];
                Debug.Log("Player " + ID + " first move & found " + types.Length + " combinations");
                return newComb;
            }

            CombinationTypes type = currentPlay.type;
            CardCombination[] arr = combinations[type].ToArray();
            CardCombination comb = null;
            if (type != CombinationTypes.Invalid && arr.Length > 0)
            {

                if (currentPlay.type == CombinationTypes.Single || currentPlay.type == CombinationTypes.Pair || currentPlay.type == CombinationTypes.Triples)
                {
                    CardCombination _comb = arr.FirstOrDefault(x => x.cards[0].value > currentPlay.cards[0].value);
                    if (_comb != null) comb = _comb;
                    else return null;
                }
                else if (currentPlay.type > CombinationTypes.Triples)
                {
                    CardCombination _comb = arr.FirstOrDefault(x => x > currentPlay);
                    if (_comb != null) comb = _comb;
                    else return null;
                }
            }

            return comb;
        }

        public void ButtonSelectCombination(int index)
        {
            CombinationTypes ct = (CombinationTypes)index;
            CardCombination[] cc = combinations[ct].ToArray();
            qhIndex++;
            if (qhType != ct || qhIndex > cc.Length - 1)
            {
                qhType = ct;
                qhIndex = 0;
            }

            Card[] selected = combinations[ct][qhIndex].cards;
            foreach (Card card in selectedCards) card.ToggleCard(false);
            foreach (Card card in selected) card.ToggleCard(true);


            selectedCards = selected.ToList();
            selectedCombination = combinations[ct][qhIndex];
        }

        public void SubmitCard()
        {
            if (GameManager.Instance.playerTurn != ID) return;
            if (selectedCombination != null)
            {
                bool isValid = GameManager.Instance.isFirstMove || selectedCombination > CardManager.Instance.currentPlay;

                if (isValid)
                {
                    animator?.SetTrigger("Attacking");
                    attackParticle.gameObject.SetActive(true);
                    if (GameManager.Instance.lastDealer != this)
                        GameManager.Instance.lastDealer?.Hit();

                    CardCombination comb = selectedCombination;
                    foreach (Card card in selectedCombination.cards)
                    {
                        cards.Remove(card);
                        cardValues.Remove((int)card.value);
                        card.ToggleCard(false);
                    }

                    GameManager.Instance.lastDealer = this;
                    CardManager.Instance.DealCard(comb);

                    selectedCombination = null;
                    selectedCards.Clear();

                    Debug.Log("Player " + ID + " have " + cards.Count + " left");
                    if (cards.Count <= 0) GameManager.Instance.SetWinner(this);
                    else GameManager.Instance.NextTurn();
                }
                else
                    Debug.Log("Player " + ID + " trying to deal invalid cards");
            }
        }

        public void PassTurn()
        {
            Debug.Log("Player " + ID + " pass");
            if (GameManager.Instance.roundWinner == this)
                GameManager.Instance.roundWinner = null;

            isPassed = true;
            stateMachine.ChangeState(dieState);
            GameManager.Instance.NextTurn(false);
        }

        private void ResetHand()
        {
            foreach (Card card in cards)
                CardManager.Instance.cardPool.Return(card);
            cardValues.Clear();
            cards.Clear();
            attackParticle.gameObject.SetActive(false);
            hitParticle.gameObject.SetActive(false);
            stateMachine.ChangeState(attackState);
            character.SwitchRig(false);
        }

        public void Hit()
        {
            animator?.SetTrigger("Hit");
            hitParticle.gameObject.SetActive(true);
        }
    }
}
