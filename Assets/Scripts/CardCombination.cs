using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using static BigTwoBattle.Constants;

namespace BigTwoBattle
{
    [Serializable]
    public class CardCombination : IEquatable<CardCombination>, IComparable<CardCombination>
    {
        public Card value;
        public Card[] cards;
        public CombinationTypes type;

        public static CardCombination MakeCombination(Card[] cards, Card value = null)
        {
            Card[] _cards = cards;
            if (CardValidator.IsFullHouse(cards))
            {
                _cards = cards.GroupBy(x => x.value)
                                .OrderByDescending(g => g.Count())
                                .ThenBy(g => g.Key)
                                .SelectMany(x => x)
                                .ToArray();
                return new CardCombination(_cards, CombinationTypes.FullHouse, cards[0]);
            }
            return new CardCombination(_cards, CardValidator.Validate(_cards));
        }

        public static CardCombination MakeCombination(Card[] cards, CombinationTypes type, Card value = null)
        {
            return new CardCombination(cards, type);
        }

        public CardCombination(Card[] cards, CombinationTypes type, Card value = null)
        {
            this.cards = cards;
            this.type = type;
            this.value = value ?? cards.Length > 0 ? cards[0] : null;
        }

        public void MoveParent(Transform parent, bool isReturn = false)
        {
            foreach (Card item in cards)
            {
                item.transform.SetParent(parent, false);
                if (isReturn) CardManager.Instance.cardPool.Return(item);
            }
        }

        public bool Equals(CardCombination other) => cards == other.cards && type == other.type;
        public static bool operator >(CardCombination left, CardCombination right) => left.CompareTo(right) > 0;
        public static bool operator <(CardCombination left, CardCombination right) => left.CompareTo(right) < 0;
        public int CompareTo(CardCombination other)
        {
            if (type == other.type)
                return value.CompareTo(other.value);
            else
                return type - other.type;
        }

    }
}