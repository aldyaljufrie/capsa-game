using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BigTwoBattle
{
    public class Avatar : MonoBehaviour
    {
        private Player owner;
        private Transform indicator;
        [HideInInspector] public RawImage image;

        private void Awake()
        {
            indicator = transform.GetChild(0);
            owner = GetComponentInParent<Player>();
            image = GetComponent<RawImage>();
        }

        private void Update()
        {
            indicator.gameObject.SetActive(GameManager.Instance.playerTurn == owner.ID);
        }
    }
}
