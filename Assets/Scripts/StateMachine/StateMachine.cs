using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BigTwoBattle
{
    public class StateMachine
    {
        public HeroState currentState { get; set; }

        public void Init(HeroState state)
        {
            currentState = state;
            currentState.EnterState();
        }

        public void ChangeState(HeroState state)
        {
            currentState.ExitState();
            currentState = state;
            currentState.EnterState();
        }
    }
}
