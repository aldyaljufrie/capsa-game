using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BigTwoBattle
{
    public class HeroState
    {
        protected Hero hero;
        protected StateMachine stateMachine;

        public HeroState(Hero hero, StateMachine stateMachine)
        {
            this.hero = hero;
            this.stateMachine = stateMachine;
        }

        public virtual void EnterState() { }
        public virtual void ExitState() { }
        public virtual void FrameUpdate() { }
        public virtual void AnimationTrigger(Hero.AnimationTriggerType type) { }
    }
}
