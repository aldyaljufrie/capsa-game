using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.UI;

namespace BigTwoBattle
{
    public class Hero : MonoBehaviour
    {
        public enum AnimationTriggerType
        {
            Attack,
            Hit,
            Die
        }
        public int ID = 0;
        protected Avatar avatar;
        [HideInInspector] public Animator animator;
        [HideInInspector] public Character character;
        public StateMachine stateMachine;
        public IdleState idleState;
        public AttackState attackState;
        public DieState dieState;


        protected virtual void Awake()
        {
            stateMachine = new StateMachine();
            idleState = new IdleState(this, stateMachine);
            attackState = new AttackState(this, stateMachine);
            dieState = new DieState(this, stateMachine);
            avatar = GetComponentInChildren<Avatar>();
        }

        protected virtual void Start() => stateMachine.Init(attackState);

        protected void AnimationTriggerEvent(AnimationTriggerType type)
        {
            stateMachine.currentState.AnimationTrigger(type);
        }

        public void SetCharacter(Character _character)
        {
            character = _character;
            animator = character.GetComponent<Animator>();
            avatar.image.texture = character.renderTexture;
            character.SwitchRig(false);
        }
    }
}