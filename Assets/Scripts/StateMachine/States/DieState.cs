using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BigTwoBattle
{
    public class DieState : HeroState
    {
        public DieState(Hero hero, StateMachine stateMachine) : base(hero, stateMachine)
        {
        }

        public override void EnterState()
        {
            base.EnterState();
            hero.animator?.SetBool("Idle", true);
        }

        public override void ExitState()
        {
            base.ExitState();
            hero.animator?.SetBool("Idle", false);
        }

        public override void FrameUpdate()
        {
            base.FrameUpdate();
        }

        public override void AnimationTrigger(Hero.AnimationTriggerType type)
        {
            base.AnimationTrigger(type);
        }
    }
}
