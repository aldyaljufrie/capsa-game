using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BigTwoBattle
{
    public class AttackState : HeroState
    {
        public AttackState(Hero hero, StateMachine stateMachine) : base(hero, stateMachine)
        {
        }

        public override void EnterState()
        {
            base.EnterState();
            hero.animator?.SetBool("Attack", true);
        }

        public override void ExitState()
        {
            base.ExitState();
            hero.animator?.SetBool("Attack", false);
        }

        public override void FrameUpdate()
        {
            base.FrameUpdate();
        }

        public override void AnimationTrigger(Hero.AnimationTriggerType type)
        {
            base.AnimationTrigger(type);
        }
    }
}
