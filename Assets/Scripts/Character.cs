using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BigTwoBattle
{
    public class Character : MonoBehaviour
    {
        public Transform portrait, preview;
        public RenderTexture renderTexture;
        private Camera cam;

        private void Awake()
        {
            cam = GetComponentInChildren<Camera>();
            renderTexture = new RenderTexture(512, 512, 16, RenderTextureFormat.ARGB32);
            renderTexture.Create();
            cam.targetTexture = renderTexture;
        }

        private void OnDestroy()
        {
            renderTexture.Release();
        }

        public void SwitchRig(bool isPreview)
        {
            cam.transform.SetParent(isPreview ? preview : portrait, false);
            cam.backgroundColor = new Color(cam.backgroundColor.r, cam.backgroundColor.g, cam.backgroundColor.b, isPreview ? 0 : 1);
        }
    }
}
