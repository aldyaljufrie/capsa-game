using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using static BigTwoBattle.Constants;
using System;

namespace BigTwoBattle
{
    public class UIManager : MonoBehaviour
    {
        public static UIManager Instance { get; private set; }
        [Header("Menu UI")]
        [SerializeField] private Transform heroesRig;
        [SerializeField] private Character[] heroes;
        [SerializeField] private RawImage heroPreview;
        [SerializeField] private TextMeshProUGUI heroName;
        private int heroIndex = 0;

        [Header("Game UI")]
        [SerializeField] private Button submitButton;
        [SerializeField] private Button passButton;
        [SerializeField] private Transform quickBars;
        [SerializeField] private GameObject handTextImage;
        private TextMeshProUGUI handText;
        private Button[] quickBtns;

        [Header("Game Over UI")]
        [SerializeField] private RawImage gameOverPreview;
        [SerializeField] private TextMeshProUGUI gameOverTitle;

        private void Awake()
        {
            if (Instance != null && Instance != this)
                Destroy(this);
            else
                Instance = this;

            quickBtns = quickBars.GetComponentsInChildren<Button>(true);
            heroes = heroesRig.GetComponentsInChildren<Character>();
            handText = handTextImage.GetComponentInChildren<TextMeshProUGUI>();
        }

        private void Start()
        {
            heroPreview.texture = heroes[0].renderTexture;
            SceneManager.Instance.OnChangeScene += ChangeScene;
            GameManager.Instance.players[0].OnCombinationChanged += UpdateButton;
        }

        private void OnDisable()
        {
            SceneManager.Instance.OnChangeScene += ChangeScene;
            GameManager.Instance.players[0].OnCombinationChanged -= UpdateButton;
        }

        private void ChangeScene(SceneManager.Scene scene)
        {
            if (scene == SceneManager.Scene.Game) SetAvatars();
            if (scene == SceneManager.Scene.GameOver)
            {
                GameManager.Instance.gameWinner.character.SwitchRig(true);
                gameOverPreview.texture = GameManager.Instance.gameWinner.character.renderTexture;
                gameOverTitle.text = "Player " + GameManager.Instance.gameWinner.ID + " Win!";
            }
        }

        private void Update()
        {
            if (CardManager.Instance.currentPlay?.cards.Length > 0 &&
                CardManager.Instance.currentPlay?.type != CombinationTypes.Single)
            {
                handTextImage.SetActive(true);
                handText.text = CardManager.Instance.currentPlay.type.ToString();
            }
            else
                handTextImage.SetActive(false);

            bool flag = GameManager.Instance.playerTurn == 0 && !GameManager.Instance.players[0].isPassed;
            submitButton.gameObject.SetActive(flag);
            passButton.gameObject.SetActive(flag);
            quickBars.gameObject.SetActive(flag);

            if (GameManager.Instance.playerTurn != 0)
                foreach (Button btn in quickBtns)
                    btn.gameObject.SetActive(false);
        }

        private void UpdateButton(Dictionary<CombinationTypes, List<CardCombination>> combs)
        {
            if (GameManager.Instance.players[0].isPassed) return;

            CombinationTypes[] keys = combs.Where(i => i.Value.Count > 0).ToDictionary(i => i.Key, i => i.Value).Keys.ToArray();

            if (!GameManager.Instance.isFirstMove)
                keys = keys.Where(i => i == CardManager.Instance.currentPlay.type).ToArray();

            if (keys.Length > 0)
            {
                for (int i = 0; i < keys.Length; i++)
                {
                    int index = Array.FindIndex(quickBtns, x => x.gameObject.name == keys[i].ToString());
                    if (index >= 0 && index < quickBtns.Length)
                        quickBtns[index]?.gameObject.SetActive(true);
                }
            }
        }

        public void NextHero()
        {
            heroIndex++;
            if (heroIndex > heroes.Length - 1)
                heroIndex = heroes.Length - 1;

            heroPreview.texture = heroes[heroIndex].renderTexture;
            heroName.text = heroes[heroIndex].gameObject.name;
        }

        public void PrevHero()
        {
            heroIndex--;
            if (heroIndex <= 0) heroIndex = 0;

            heroPreview.texture = heroes[heroIndex].renderTexture;
            heroName.text = heroes[heroIndex].gameObject.name;
        }

        public void SetAvatars()
        {
            Character selected = heroes[heroIndex];
            Character[] rest = heroes.Where(x => x != selected).ToArray();
            Shuffle(rest);
            for (int i = 0; i < GameManager.Instance.players.Length; i++)
            {
                if (i == 0)
                    GameManager.Instance.players[i].SetCharacter(selected);
                else
                    GameManager.Instance.players[i].SetCharacter(rest[i]);
            }
        }
    }
}
