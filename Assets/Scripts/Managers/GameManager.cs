using System;
using System.Collections;
using UnityEngine;

namespace BigTwoBattle
{
    public enum GameState
    {
        Ready,
        Play,
        Gameover
    }

    public class GameManager : MonoBehaviour
    {
        public static GameManager Instance { get; private set; }
        public GameState state = GameState.Ready;
        public Player[] players;
        public Player lastDealer, roundWinner, gameWinner;
        public int round = 1;
        public int playerTurn = -1;
        public bool isFirstMove = true;
        public event Action<int, bool> OnNextTurn;
        public event Action<GameState> OnGameStateChanged;

        private void Awake()
        {
            if (Instance != null && Instance != this)
                Destroy(this);
            else
                Instance = this;
        }

        public void ChangeGameState(GameState newState)
        {
            Debug.Log("New Game State = " + newState.ToString());
            state = newState;

            if (newState == GameState.Play)
            {
                round = 1;
                playerTurn = -1;
                lastDealer = null;
                roundWinner = null;
                gameWinner = null;
            }

            OnGameStateChanged?.Invoke(newState);
        }

        public void SetFirstTurn(int id)
        {
            playerTurn = id;
            roundWinner = players[id];
        }

        public void NextTurn(bool deal = true)
        {
            StopAllCoroutines();
            StartCoroutine(NextTurnCoroutine(deal));
        }

        IEnumerator NextTurnCoroutine(bool deal)
        {
            playerTurn++;
            if (playerTurn >= players.Length)
                playerTurn = 0;

            yield return new WaitForSeconds(players[playerTurn].isPassed ? 0 : 0.5f);

            if (!deal)
            {
                if (lastDealer == players[playerTurn])
                {
                    isFirstMove = true;
                    roundWinner = players[playerTurn];
                    round++;
                    CardManager.Instance.NewRound();
                    Debug.Log("Player " + playerTurn + "'s turn, New Round");
                    OnNextTurn?.Invoke(playerTurn, true);
                    yield break;
                }
            }

            isFirstMove = false;
            Debug.Log("Player " + playerTurn + "'s turn");
            OnNextTurn?.Invoke(playerTurn, false);
        }

        public void SetWinner(Player winner)
        {
            Debug.Log("Player " + winner.ID + " is the winner!");
            gameWinner = winner;
            StartCoroutine(SetWinnerCoroutine());
        }

        IEnumerator SetWinnerCoroutine()
        {
            yield return new WaitForSeconds(1.5f);
            SceneManager.Instance.ChangeScene(2);
        }
    }
}
