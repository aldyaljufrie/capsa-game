using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using static BigTwoBattle.Constants;

namespace BigTwoBattle
{
    public class CardManager : MonoBehaviour, IObjectPool<Card>
    {
        public static CardManager Instance { get; private set; }

        public CardCombination lastPlay, currentPlay;
        [SerializeField]
        private Transform[] boardHand;
        [SerializeField]
        private bool randomSeed = true;
        [SerializeField]
        private string seed = "0";
        [SerializeField]
        private AssetReferenceTexture2D cardAsset;
        private Dictionary<string, Sprite> cardSprites = new();
        public ObjectPool<Card> cardPool;
        public Card prefab;
        public delegate void OnReset();
        public event OnReset onReset;
        public event Action OnDealCard;

        private void Awake()
        {
            if (Instance != null && Instance != this)
                Destroy(this);
            else
                Instance = this;

            cardPool = new ObjectPool<Card>(Spawn, OnReceived, OnReturned, 52);
            InitSeed();
        }

        private void Start()
        {
            cardAsset.LoadAssetAsync<IList<Sprite>>().Completed += LoadSprites;
            GameManager.Instance.OnGameStateChanged += GameStateChanged;
        }

        private void OnDisable()
        {
            GameManager.Instance.OnGameStateChanged -= GameStateChanged;
        }

        private void GameStateChanged(GameState state)
        {
            if (state == GameState.Play)
                InitializeCards();
            if (state == GameState.Gameover)
                ResetBoard();

        }

        private void InitSeed()
        {
            if (randomSeed)
            {
                seed = "";
                for (int i = 0; i < 20; i++)
                    seed += Glyphs[UnityEngine.Random.Range(0, Glyphs.Length)];
            }

            UnityEngine.Random.InitState(seed.GetHashCode());
        }

        public void InitializeCards()
        {
            List<int> cardIndex = new List<int>();
            for (int i = 0; i < 52; i++)
                cardIndex.Add(i);

            List<int> deck = new List<int>();
            for (int i = 0; i < 52; i++)
            {
                int index = UnityEngine.Random.Range(0, cardIndex.Count);
                int value = cardIndex[index];
                deck.Add(value);
                cardIndex.Remove(value);
            }

            for (int i = 0; i < 4; i++)
            {
                Player player = GameManager.Instance.players[i];
                int cardsCount = deck.Count;
                int start = cardsCount - 13;

                List<int> values = deck.GetRange(start, 13);
                values.Sort();
                player.SetPlayerHand(values);
                deck.RemoveRange(start, 13);
            }
        }

        public void ResetBoard()
        {
            onReset?.Invoke();
            InitSeed();
            NewRound();
        }

        public void DealCard(CardCombination cc)
        {
            Debug.Log("Player " + GameManager.Instance.playerTurn + " deals " + DebugCombination(cc));

            if (lastPlay?.cards.Length > 0)
            {
                DebugCombination(lastPlay);
                lastPlay?.MoveParent(transform);
                foreach (Card c in lastPlay.cards)
                    cardPool.Return(c);
            }
            if (currentPlay?.cards.Length > 0)
            {
                currentPlay?.MoveParent(boardHand[1]);
                lastPlay = currentPlay;
            }

            cc.MoveParent(boardHand[0]);
            currentPlay = cc;

            foreach (Card c in cc.cards)
                c.FlipCard(true);

            OnDealCard?.Invoke();
        }

        public void NewRound()
        {
            lastPlay.MoveParent(transform, true);
            currentPlay.MoveParent(transform, true);
            lastPlay = CardCombination.MakeCombination(new Card[] { });
            currentPlay = CardCombination.MakeCombination(new Card[] { });
        }

        private void LoadSprites(AsyncOperationHandle<IList<Sprite>> handle)
        {
            Debug.Log("Load Sprites");
            if (handle.Status == AsyncOperationStatus.Succeeded)
            {
                foreach (Sprite item in handle.Result)
                    cardSprites[item.name] = item;
            }
            else
                Debug.Log("Failed to load card asset!");
        }

        public Sprite GetSprite(string name)
        {
            return cardSprites[name];
        }

        public Card Spawn()
        {
            Card card = Instantiate(prefab);
            card.transform.SetParent(transform, false);
            return card;
        }
        public void OnReceived(Card obj)
        {
            obj.gameObject.SetActive(true);
            obj.EnableCard();
        }
        public void OnReturned(Card obj)
        {
            obj.GetComponentInChildren<Image>().color = Color.white;
            obj.gameObject.SetActive(false);
            obj.DisableCard();
        }

        public string DebugCombination(CardCombination comb)
        {
            string log = "";
            log += comb.type.ToString() + ", ";
            foreach (Card card in comb.cards)
                log += card.value.ToString() + "_" + card.suit.ToString() + ", ";
            return log;
        }
    }
}