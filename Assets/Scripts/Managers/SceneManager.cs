using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BigTwoBattle
{
    public class SceneManager : MonoBehaviour
    {
        public enum Scene
        {
            Menu = 0,
            Game = 1,
            GameOver = 2
        }
        public static SceneManager Instance { get; private set; }
        public Scene scene = Scene.Game;
        public GameObject[] scenes;
        private Camera cam;
        public event Action<Scene> OnChangeScene;
        private void Awake()
        {
            if (Instance != null && Instance != this)
                Destroy(this);
            else
                Instance = this;

            cam = Camera.main;
        }

        private void Start()
        {
            ChangeScene(0);
        }

        private void OnEnable()
        {
            OnChangeScene += OnSceneChanged;
        }

        private void OnDisable()
        {
            OnChangeScene -= OnSceneChanged;
        }

        public void ChangeScene(int index)
        {
            scene = (Scene)index;
            OnChangeScene?.Invoke(scene);
        }

        private void OnSceneChanged(Scene newScene)
        {
            for (int i = 0; i < scenes.Length; i++)
                scenes[i].SetActive((int)newScene == i);

            if (newScene == Scene.Game)
                StartCoroutine(AnimCamera(GameState.Play, 10f));
            if (newScene == Scene.GameOver)
                StartCoroutine(AnimCamera(GameState.Gameover, 13f));
        }

        private IEnumerator AnimCamera(GameState gameState, float end)
        {
            float lerpDuration = 0.75f;
            float timeElapsed = 0;
            float start = cam.orthographicSize;

            while (timeElapsed < lerpDuration)
            {
                float t = timeElapsed / lerpDuration;
                t = t * t * (3f - 2f * t);
                cam.orthographicSize = Mathf.Lerp(start, end, t);
                timeElapsed += Time.deltaTime;
                yield return null;
            }
            GameManager.Instance.ChangeGameState(gameState);
        }
    }
}
