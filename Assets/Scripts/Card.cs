using System;
using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using static BigTwoBattle.Constants;

namespace BigTwoBattle
{
    public class Card : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler, ISelectHandler, IDeselectHandler, IComparable<Card>, IEquatable<Card>
    {
        // Simple card factory
        public static Card Create(int value, Player owner, Transform parent)
        {
            Card card = CardManager.Instance.cardPool.Get();
            card.owner = owner;
            card.SetCardValue(value);
            card.GetComponent<RectTransform>().sizeDelta = cardSizes[!owner.isAI ? 0 : 1];
            card.transform.SetParent(parent, false);
            card.EnableCard();
            return card;
        }

        public Suits suit;
        public Values value;
        [SerializeField] private RectTransform container;
        [SerializeField] private Image outline;
        public Image image;
        private Player owner;
        public bool selected = false;
        private static readonly Vector2[] cardSizes = { new Vector2(125, 195), new Vector2(65, 100) };

        public void EnableCard()
        {
            if (!owner) return;

        }

        public void DisableCard()
        {
            if (!owner) return;

        }

        public void ToggleCard(bool flag)
        {
            selected = flag;
            outline.gameObject.SetActive(selected);
            StartCoroutine(MoveCard(selected));
        }

        public void SetCardValue(int v)
        {
            value = (Values)(v / 4 + 1);
            suit = (Suits)(v % 4 + 1);
            FlipCard(!owner.isAI);
        }

        public void Reset(Card[] c)
        {
            Card item = Array.Find(c, i => i == this);
            selected = item;
        }

        public void FlipCard(bool flag)
        {
            image.sprite = CardManager.Instance.GetSprite(flag ? suit.ToString() + "_" + value : "Back_Orange");
        }

        public bool Equals(Card other) => suit == other.suit && value == other.value;
        public static bool operator >(Card left, Card right) => left.CompareTo(right) > 0;
        public static bool operator <(Card left, Card right) => left.CompareTo(right) < 0;
        public int CompareTo(Card other)
        {
            if (value == other.value)
                return suit - other.suit;
            else
                return value - other.value;
        }

        IEnumerator MoveCard(bool select)
        {
            float elapsedTIme = 0f;
            while (elapsedTIme < 0.1f)
            {
                elapsedTIme += Time.deltaTime;
                float endOffset = select ? 20f : 0f;

                float offset = Mathf.Lerp(container.offsetMin.y, endOffset, elapsedTIme / 0.1f);
                container.offsetMax = new Vector2(container.offsetMax.x, offset);
                container.offsetMin = new Vector2(container.offsetMin.x, offset);

                yield return null;
            }
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (owner.isAI) return;

            if (!selected) owner.AddSelectedCard(this);
            else owner.RemoveSelectedCard(this);
        }

        public void OnPointerEnter(PointerEventData eventData) => eventData.selectedObject = gameObject;
        public void OnPointerExit(PointerEventData eventData) => eventData.selectedObject = null;

        public void OnSelect(BaseEventData eventData)
        {
            StartCoroutine(MoveCard(true));
        }

        public void OnDeselect(BaseEventData eventData)
        {
            if (!selected)
                StartCoroutine(MoveCard(false));
        }
    }
}
