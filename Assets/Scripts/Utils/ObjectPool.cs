using System;
using System.Collections.Generic;
using UnityEngine;

namespace BigTwoBattle
{
    public class ObjectPool<T>
    {
        private readonly List<T> pool;
        private readonly Func<T> spawn;
        private readonly bool isDynamic;
        private readonly Action<T> OnReceived;
        private readonly Action<T> OnReturned;

        public ObjectPool(Func<T> spawn, Action<T> OnReceived, Action<T> OnReturned, int initialSize = 0, bool isDynamic = true)
        {
            this.spawn = spawn;
            this.isDynamic = isDynamic;
            this.OnReceived = OnReceived;
            this.OnReturned = OnReturned;

            pool = new List<T>();
            for (var i = 0; i < initialSize; i++)
            {
                var o = this.spawn();
                this.OnReturned(o);
                pool.Add(o);
            }
        }

        public T Get()
        {
            var result = default(T);
            if (pool.Count > 0)
            {
                result = pool[0];
                pool.RemoveAt(0);
            }
            else if (isDynamic)
                result = spawn();
            OnReceived(result);
            return result;
        }

        public void Return(T o)
        {
            OnReturned(o);
            pool.Add(o);
        }
    }
}
