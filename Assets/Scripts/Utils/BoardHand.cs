using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

namespace BigTwoBattle
{
    public class BoardHand : MonoBehaviour
    {
        [SerializeField] private Color cardColor;
        [SerializeField] private float childSize = 160f;
        private RectTransform rectTransform;

        private void Awake()
        {
            rectTransform = GetComponent<RectTransform>();
        }

        private void OnEnable()
        {
            CardManager.Instance.OnDealCard += OnDealCard;
        }

        private void OnDisable()
        {
            CardManager.Instance.OnDealCard -= OnDealCard;
        }

        private void OnDealCard()
        {
            if (transform.childCount <= 0) return;

            rectTransform.sizeDelta = new Vector2(childSize * transform.childCount, rectTransform.sizeDelta.y);

            Card[] cards = GetComponentsInChildren<Card>();

            if (cards.Length > 0)
                foreach (Card cf in cards)
                    cf.image.color = cardColor;
        }
    }
}
