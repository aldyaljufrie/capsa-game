using UnityEngine;

namespace BigTwoBattle
{
    public interface IObjectPool<T>
    {
        public T Spawn();
        public void OnReceived(T obj);
        public void OnReturned(T obj);
    }
}
