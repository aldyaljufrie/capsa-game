using UnityEngine;

namespace BigTwoBattle
{
    public static class Constants
    {
        public static T[] Shuffle<T>(T[] array)
        {
            int p = array.Length;
            for (int n = p - 1; n > 0; n--)
            {
                int r = Random.Range(1, n);
                (array[n], array[r]) = (array[r], array[n]);
            }
            return array;
        }

        public const string Glyphs = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890!@#$%^&*()";
        public enum Suits
        {
            Diamond = 1,
            Club = 2,
            Heart = 3,
            Spade = 4,
        }

        public enum Values
        {
            Three = 1,
            Four = 2,
            Five = 3,
            Six = 4,
            Seven = 5,
            Eight = 6,
            Nine = 7,
            Ten = 8,
            Jack = 9,
            Queen = 10,
            King = 11,
            Ace = 12,
            Two = 13,
        }

        public enum CombinationTypes
        {
            Invalid,
            Single,
            Pair,
            Triples,
            FourOfAKind,
            FourOfAKindPlus,
            Flush,
            Straight,
            FullHouse,
            StraightFlush
        }
    }

}