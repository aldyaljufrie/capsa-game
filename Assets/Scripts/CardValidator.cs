using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using static BigTwoBattle.Constants;

namespace BigTwoBattle
{
    public class CardValidator
    {
        public static List<CardCombination> FindPairs(List<Card> cards)
        {
            Values lastValue = 0;
            List<CardCombination> pairs = new();
            for (int i = 0; i < cards.Count; i++)
            {
                if (i > cards.Count - 2) break;
                if (cards[i].value == lastValue) continue;

                Card[] dupes = cards.FindAll(c => c.value == cards[i].value).ToArray();
                if (dupes.Length > 1)
                    for (int j = 0; j < dupes.Length; j++)
                    {
                        Card key = dupes[j];
                        for (int k = j; k < dupes.Length; k++)
                        {
                            if (key == dupes[k]) continue;

                            Card[] pair = { key, dupes[k] };
                            if (IsValidPairs(pair))
                                pairs.Add(CardCombination.MakeCombination(pair, CombinationTypes.Pair));

                        }
                    }

                lastValue = cards[i].value;
            }
            return pairs;
        }

        public static List<CardCombination> FindTriples(List<Card> cards)
        {
            Values lastValue = 0;
            List<CardCombination> triples = new();
            for (int i = 0; i < cards.Count; i++)
            {
                if (i > cards.Count - 3) break;
                if (cards[i].value == lastValue) continue;

                Card[] dupes = cards.FindAll(c => c.value == cards[i].value).ToArray();
                if (dupes.Length > 2)
                    for (int j = 1; j < dupes.Length; j++)
                    {
                        Card key = dupes[j];
                        for (int k = j; k < dupes.Length; k++)
                        {
                            if (key == dupes[k]) continue;

                            Card[] triple = { dupes[0], key, dupes[k] };
                            if (triple.Length == 3 && IsValidPairs(triple))
                                triples.Add(CardCombination.MakeCombination(triple, CombinationTypes.Triples));
                        }
                    }
                if (dupes.Length == 4)
                {
                    Card[] triple = { dupes[1], dupes[2], dupes[3] };
                    if (triple.Length == 3 && IsValidPairs(triple))
                        triples.Add(CardCombination.MakeCombination(triple));
                }

                lastValue = cards[i].value;
            }
            return triples;
        }

        public static List<CardCombination> FindFourKind(List<Card> cards)
        {
            List<CardCombination> fourKind = new();
            for (int i = 0; i < cards.Count; i++)
            {
                if (i > cards.Count - 4) break;

                Card[] four = { cards[i], cards[i + 1], cards[i + 2], cards[i + 3] };
                if (IsValidPairs(four))
                    fourKind.Add(CardCombination.MakeCombination(four));
            }
            return fourKind;
        }

        public static List<CardCombination> FindFlush(List<Card> cards)
        {
            List<Card> five = new();
            List<CardCombination> flush = new();
            for (int i = 0; i < 4; i++)
            {
                var suits = Enum.GetValues(typeof(Suits)).Cast<Suits>().ToArray();
                Card[] flushes = cards.FindAll(c => c.suit == suits[i]).ToArray();
                for (int j = 0; j < flushes.Length; j++)
                {
                    if (five.Count > 0)
                    {
                        five.Add(flushes[j]);
                        if (five.Count == 5)
                        {
                            if (IsFlush(five.ToArray()))
                                flush.Add(CardCombination.MakeCombination(five.ToArray(), CombinationTypes.Flush));
                            five.RemoveAt(0);
                        }
                    }
                    else
                        five.Add(flushes[j]);
                }
            }
            return flush;
        }

        public static List<CardCombination> FindFullHouse(CardCombination[] triples, CardCombination[] pairs)
        {
            List<CardCombination> fullhouse = new();
            foreach (CardCombination t in triples)
            {
                Card[] triple = t.cards.ToArray();
                foreach (CardCombination p in pairs)
                {
                    Card[] pair = p.cards.ToArray();
                    Card[] fh = triple.Concat(pair).OrderByDescending(x => x.value == t.cards[0].value).ToArray();
                    if (IsFullHouse(fh))
                        fullhouse.Add(CardCombination.MakeCombination(fh, CombinationTypes.FullHouse, t.cards[0]));
                }
            }
            return fullhouse;
        }

        public static List<CardCombination> FindStraight(List<Card> cards)
        {
            List<Card> five = new();
            List<CardCombination> straight = new();
            for (int i = 0; i < cards.Count; i++)
            {
                if (five.Count > 0)
                {
                    int diff = (int)cards[i].value - (int)five[^1].value;
                    if (diff == 1)
                    {
                        five.Add(cards[i]);
                        if (five.Count == 5)
                        {
                            if (IsStraight(five.ToArray()))
                                straight.Add(CardCombination.MakeCombination(five.ToArray(), CombinationTypes.Straight));
                            five.RemoveAt(0);
                        }
                    }
                    else if (diff > 1)
                        five.Clear();
                }
                else
                    five.Add(cards[i]);
            }

            return straight;
        }

        public static List<CardCombination> FindStraightFlush(List<Card> cards)
        {
            List<CardCombination> straightFlush = new();
            for (int i = 0; i < 4; i++)
            {
                var suits = Enum.GetValues(typeof(Suits)).Cast<Suits>().ToArray();
                Card[] five = cards.FindAll(c => c.suit == suits[i]).ToArray();
                if (five.Length >= 5)
                {
                    List<CardCombination> straight = FindStraight(five.ToList());
                    foreach (CardCombination s in straight)
                    {
                        Card[] sf = s.cards.ToArray();
                        if (IsStraightFlush(sf.ToArray()))
                            straightFlush.Add(CardCombination.MakeCombination(sf.ToArray(), CombinationTypes.StraightFlush));
                    }
                }
            }
            return straightFlush;
        }

        public static CombinationTypes Validate(Card[] cards)
        {
            CombinationTypes type = CombinationTypes.Invalid;

            switch (cards.Length)
            {
                case 1:
                    type = CombinationTypes.Single;
                    break;
                case 2:
                    if (IsValidPairs(cards)) type = CombinationTypes.Pair;
                    break;
                case 3:
                    if (IsValidPairs(cards)) type = CombinationTypes.Triples;
                    break;
                case 4:
                    if (IsValidPairs(cards)) type = CombinationTypes.FourOfAKind;
                    break;
                case 5:
                    type = ValidateFiveSet(cards);
                    break;
            }

            return type;
        }

        public static bool IsValidPairs(Card[] cards)
        {
            Array.Sort(cards, (x, y) => x.value.CompareTo(y.value));
            return cards[0].value.Equals(cards[^1].value);
        }

        public static CombinationTypes ValidateFiveSet(Card[] cards)
        {
            CombinationTypes type = CombinationTypes.Invalid;
            // if (IsFourPlus(cards)) type = CombinationTypes.FourOfAKindPlus;
            if (IsFlush(cards)) type = CombinationTypes.Flush;
            else if (IsStraight(cards)) type = CombinationTypes.Straight;
            else if (IsFullHouse(cards)) type = CombinationTypes.FullHouse;
            else if (IsStraightFlush(cards)) type = CombinationTypes.StraightFlush;
            return type;
        }

        public static bool IsFourPlus(Card[] cards)
        {
            Array.Sort(cards, (x, y) => x.value.CompareTo(y.value));
            Dictionary<int, int> values = new();
            foreach (Card card in cards)
            {
                int cardValue = (int)card.value;
                if (values.ContainsKey(cardValue))
                {
                    int value = values[cardValue];
                    values[cardValue] = value + 1;
                }
                else
                    values[cardValue] = 1;
            }

            var flag = values.Count == 2 && values.ContainsValue(4) && values.ContainsValue(1);
            return flag;
        }

        public static bool IsStraight(Card[] cards)
        {
            Array.Sort(cards, (x, y) => x.value.CompareTo(y.value));
            for (int i = 1; i < cards.Length; i++)
                if ((int)cards[i].value - (int)cards[i - 1].value != 1) return false;
            return true;
        }

        public static bool IsFlush(Card[] cards)
        {
            Array.Sort(cards, (x, y) => x.value.CompareTo(y.value));
            for (int i = 1; i < cards.Length; i++)
                if (cards[i].suit != cards[i - 1].suit) return false;
            return true;
        }

        public static bool IsStraightFlush(Card[] cards)
        {
            return IsStraight(cards) && IsFlush(cards);
        }

        public static bool IsFullHouse(Card[] cards)
        {
            Dictionary<int, int> values = new();
            foreach (Card card in cards)
            {
                var cardValue = (int)card.value;
                if (values.ContainsKey(cardValue))
                {
                    var value = values[cardValue];
                    values[cardValue] = value + 1;
                }
                else
                    values[cardValue] = 1;
            }

            var flag = values.Count == 2 && values.ContainsValue(3) && values.ContainsValue(2);
            return flag;
        }
    }
}
